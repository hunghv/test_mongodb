<div id="dia_chi_content">
    <div class="tab-content">
        <div id="home" class="tab-pane active">
            <table>
                <tbody>
                <h3>TEKA Miền Nam</h3>
                <tr>
                    <td>
                        <p class="title">TEKA QUẬN 11</p>
                        1411 đường 3/2 Quận 11 - TP. Hồ Chí Minh <br />Điện thoại: <span class="sdt">(08) 3960 5795</span> <br />Hotline : <span class="sdt">0946 674 673</span><br />(Ngay gần vòng xoay cây Gõ)</td>
                    <td>
                        <p class="title">TEKA TÂN BÌNH</p>
                        591 Hoàng Văn Thụ - Q.Tân Bình - TP. HCM <br />Điện thoại: <span class="sdt">(08) 6682 5220</span> <br />Hotline : <span class="sdt">0928 97 97 97</span><br />(Đối diện Triển lãm Tân Bình)</td>
                    <td>
                        <p class="title">TEKA CỘNG HÒA</p>
                        90 Cộng Hòa - Q. Tân Bình - TP HCM <br />Điện thoại: <span class="sdt">(08) 6672 1555</span> <br />Hotline : <span class="sdt">0946 480 580</span><br />(Có nơi đậu xe ô tô miễn phí)</td>
                    <td>
                        <p class="title">TEKA BÌNH THẠNH</p>
                        76 - 78 Đinh Bộ Lĩnh - Q. Bình Thạnh - TP HCM <br />Điện thoại: <span class="sdt">(08) 6683 0878</span> <br />Hotline : <span class="sdt">0904 859 934</span><br />(Giao cắt đường Bùi Đình Túy)</td>
                </tr>
                <tr>
                    <td>
                        <p class="title">TEKA QUẬN 5</p>
                        952 đường Trần Hưng Đạo - Quận 5 - TP HCM <br />Điện thoại: <span class="sdt">(08) 6682 8335</span> <br />Hotline : <span class="sdt">0912 256 858</span><br />(Có nơi đậu xe ô tô miễn phí)</td>
                </tr>
                <h3>TEKA Miền Bắc</h3>
                <tr>
                    <td>
                        <p class="title">BEP365 398 Khâm Thiên</p>
                        398 Khâm Thiên <br />Điện thoại: <span class="sdt">(04) 6293 0184</span> <br />Hotline : <span class="sdt">0933 266 966</span></td>
                    <td>
                        <p class="title">TEKA THÁI THỊNH</p>
                        106 Thái Thịnh - Q.Đống Đa <br />Điện thoại: <span class="sdt">(04) 6683 5457</span> <br />Hotline : <span class="sdt">0933 668 569</span><br />(Cạnh KS Hacinco)</td>
                    <td>
                        <p class="title">TEKA ĐƯỜNG LÁNG</p>
                        530 đường Láng - Q.Đống Đa <br />Điện thoại: <span class="sdt">(04) 3232 1078</span> <br />Hotline : <span class="sdt">0943 83 73 63</span></td>
                    <td>
                        <p class="title">TEKA KHÂM THIÊN</p>
                        302 Khâm Thiên - Q.Đống Đa <br />Điện thoại: <span class="sdt">(04) 6260 2034</span> <br />Hotline : <span class="sdt">0948 622 922</span><br />(Có nơi đỗ xe ô tô miễn phí)</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>