<?php
namespace App\Repositories\Group;

use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Cartalyst\Sentry\Groups\NameRequiredException;
use Cartalyst\Sentry\Groups\GroupExistsException;
use Cartalyst\Sentry\Groups\GroupNotFoundException;

class EloquentGroupRepository implements GroupRepository {

    public function getAll()
    {
        $key_name = 'group_' . env('APP_SITE') . '_' . 'getall';
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $groups = Sentry::findAllGroups();
        if($groups != null) {
            cache_it($key_name, $groups);
            return $groups;
        }
        return false;
    }

    public function getById($group_id) {
        $key_name = 'group_' . env('APP_SITE') . '_' . $group_id;
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $group = Sentry::findGroupById($group_id);
        if($group != null) {
            cache_it($key_name, $group);
            return $group;
        }
        return false;
    }

    public function create($name, $permission)
    {
        try
        {
            Sentry::createGroup(array(
                'name'        => $name,
                'permissions' => $permission,
            ));
        }
        catch (NameRequiredException $e)
        {
            echo 'Name field is required';
        }
        catch (GroupExistsException $e)
        {
            echo 'Group already exists';
        }
        return false;
    }

    public function update($params)
    {
        $group = Sentry::findGroupById($params['id']);
        die('hung');
    }

    public function deleteById($group_id)
    {
        try
        {
            $group = Sentry::findGroupById($group_id);

            $group->delete();
        }
        catch (GroupNotFoundException $e)
        {
            echo 'Group was not found.';
        }
        return false;
    }
}