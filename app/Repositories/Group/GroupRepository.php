<?php
namespace App\Repositories\Group;

interface GroupRepository {

    public function getAll();

    public function getById($group_id);

    public function create($name, $permission);

    public function update($params);

    public function deleteById($id);
}