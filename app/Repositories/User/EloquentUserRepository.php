<?php
namespace App\Repositories\User;

use App\Models\User;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Groups\GroupNotFoundException;

class EloquentUserRepository implements UserRepository {

    public function getAll()
    {
        $key_name = 'user_' . env('APP_SITE') . '_' . 'getall';
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $users = Sentry::findAllUsers();
        if($users != null) {
            cache_it($key_name, $users);
            return $users;
        }
        return false;
    }

    public function getById($user_id)
    {
        $key_name = 'user_' . env('APP_SITE') . '_' . $user_id;
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $user = Sentry::findUserById($user_id);
        if($user != null) {
            cache_it($key_name, $user);
            return $user;
        }
        return false;
    }

    public function findById($user_id) {
        $key_name = 'user_' . env('APP_SITE') . '_' . $user_id;
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $user = User::find($user_id);
        if($user != null) {
            cache_it($key_name, $user);
            return $user;
        }
        return false;
    }

    public function getByEmail($user_mail) {
        $key_name = 'user_' . env('APP_SITE') . '_' . $user_mail;
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $user = User::where('email', '=', $user_mail)->first();
        if($user != null) {
            cache_it($key_name, $user);
            return $user;
        }
        return false;
    }

    public function getByCreateBy($create_by, $order, $limit) {
        $key_name = 'user_' .env('APP_SITE') . '_' . $create_by;
        $cache_value = get_from_cache($key_name);
        if($cache_value) {
            return $cache_value;
        }

        $users = User::where('create_by', '=', $create_by)
                    ->orderBy($order['col'], $order['mode'])
                    ->paginate($limit);
        if($users != null) {
            cache_it($key_name, $users);
            return $users;
        }
        return false;

    }

    public function create($params, $groups)
    {
        try {
            $user = Sentry::createUser($params);
            // SET PER
            if(!empty($params['per']))
            foreach($params['per'] as $key => $value) {
                $group_user = Sentry::findGroupByName($value);
                $user->addGroup($group_user);
            }
            // SET GROUP
            foreach($groups as $group) {
                $group_user = Sentry::findGroupByName($group);
                $user->addGroup($group_user);
            }
            if($params['activated']) $user->getActivationCode();
            return true;
        }
        catch (LoginRequiredException $e)
        {

            return 'Login field is required.';
        }
        catch (PasswordRequiredException $e)
        {
            return 'Password field is required.';
        }
        catch (UserExistsException $e)
        {
            return 'User with this login already exists.';
        }
        catch (GroupNotFoundException $e)
        {
            return 'Group was not found.';
        }
    }

    public function update($params, $groups)
    {
        try
        {
            $user = Sentry::findUserById($params['userId']);
            if($params['name'] != null) $user->name = $params['name'];
            if($params['password'] != null) $user->password = bcrypt($params['password']);
            if($params['activated'] == true) {
                $user->activated = true;
                $user->getActivationCode();
            } else {
                $user->activated = false;
            }

            $user->per = $params['per'];
            if ($user->save())
            {
                // Delete old group
                $user_group = $user->group_ids;
                if(!empty($user_group)) {
                    foreach($user_group as $value) {
                        $group_find = Sentry::findGroupById($value);
                        $user->removeGroup($group_find);
                    }
                }
                //Add new group
                foreach($groups as $group) {
                    $group_user = Sentry::findGroupByName($group);
                    $user->addGroup($group_user);
                }
                foreach($params['per'] as $key => $value) {
                    $group_user = Sentry::findGroupByName($value);
                    $user->addGroup($group_user);
                }
                return true;
            }
            else
            {
                echo 'Error!';
            }
        }
        catch (UserExistsException $e)
        {
            return 'User with this login already exists.';
        }
        catch (UserNotFoundException $e)
        {
            return 'User was not found.';
        }
    }

    public function deleteById($user_id)
    {
        try
        {
            $user = Sentry::findUserById($user_id);
            $user->delete();
            return true;
        }
        catch (UserNotFoundException $e)
        {
            return 'User was not found.';
        }
        return false;
    }

}