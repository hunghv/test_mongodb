<?php
namespace App\Repositories\User;

interface UserRepository {

    public function getAll();

    public function getById($user_id);

    public function findById($user_id);

    public function getByEmail($user_mail);

    public function getByCreateBy($create_by, $order, $limit);

    public function create($params, $groups);

    public function update($params, $groups);

    public function deleteById($user_id);
}