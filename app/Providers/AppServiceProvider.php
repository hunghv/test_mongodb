<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Repositories\User\UserRepository', 'App\Repositories\User\EloquentUserRepository');
        $this->app->singleton('App\Repositories\Group\GroupRepository', 'App\Repositories\Group\EloquentGroupRepository');
    }
}
