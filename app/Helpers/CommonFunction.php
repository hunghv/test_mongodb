<?php

if(!function_exists('get_from_cache')) {
    function get_from_cache($key) {
        if( env('APP_ENV') == 'production') {
            if(\Cache::has($key)) {
                return \Cache::get($key);
            }
            return false;
        }
    }
}

if(!function_exists('cache_it')) {
    function cache_it($key, $value) {
        if(env('APP_ENV') == 'production') {
            return \Cache::put($key, $value, env('CACHE_TIME'));
        }
        return false;
    }
}