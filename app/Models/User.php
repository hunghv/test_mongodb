<?php
namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class User extends Eloquent {

    protected $collection = 'users';

    protected $fillable = ['name', 'email', 'password', 'activated', 'create_by'];
}