<?php
namespace App\Models;

use Moloquent;

class Group extends Moloquent {

	protected $collection = 'groups';
}
