<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Validation;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\LoginRequest;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

use Symfony\Component\HttpFoundation\Session\Flash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Cartalyst\Sentry\Users\WrongPasswordException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Users\UserSuspendedException;
use Cartalyst\Sentry\Users\UserBannedException;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectHome = '/';

    protected $redirectAdmin = 'admin';

    protected $loginPath = 'auth/login';

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        try {
            // Login credentials
            $credentials = array(
                'email' => $request->email,
                'password' => $request->password
            );

            // Authenticate the user
            if (Sentry::authenticateAndRemember($credentials)) {
                $user = Sentry::getUser();
                if($user->activated) {
                    //$permissions = $user->getPermissions();
                    if ($user->hasAccess('view.admin')) {
                        return redirect('admin');
                    }else{
                        return redirect('/');
                    }
                }
            }
            return redirect('admin/login');

        } catch (LoginRequiredException $e) {
            echo 'Login field is required.';
        } catch (PasswordRequiredException $e) {
            echo 'Password field is required.';
        } catch (WrongPasswordException $e) {
            return 'Wrong password, try again.';
        } catch (UserNotFoundException $e) {
            echo 'User was not found.';
        } catch (UserNotActivatedException $e) {
            echo 'User is not activated.';
        } catch (UserSuspendedException $e) {
            echo 'User is suspended.';
        } catch (UserBannedException $e) {
            echo 'User is banned.';
        }
    }

    public function getLogout()
    {
        Sentry::logout();
        return redirect('admin/login');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
