<?php
namespace App\Http\Controllers;

use App\Repositories\User\UserRepository;
use App\Repositories\Group\GroupRepository;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Flash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{

    protected $_user_model;
    protected $_group_model;

    public $user_limit_match = 10;
    public $user_match_order = ['col' => 'created_at', 'mode' => 'DESC'];

    public function __construct(UserRepository $user, GroupRepository $group)
    {
        $this->_user_model = $user;
        $this->_group_model = $group;
    }

    public function getIndex()
    {
        $user = Sentry::getUser();
        $users = $this->_user_model->getByCreateBy($user->_id, $this->user_match_order, $this->user_limit_match);

        // Get Role users
        $list_user = array();
        foreach ($users as $member) {
            $member->groups = '';
            if ($member->group_ids)
                foreach ($member->group_ids as $key => $value) {
                    if ($this->_group_model->getById($value)->name == 'admin'
                        || $this->_group_model->getById($value)->name == 'mod'
                        || $this->_group_model->getById($value)->name == 'user'
                    ) {
                        $member->groups .= ' ' . $this->_group_model->getById($value)->name;
                    }
                }
            $list_user[] = $member;
        }

        // Get Role can be create
        $groups = $user->getGroups();
        $list_role = array(
            'user' => 'user'
        );
        foreach ($groups as $key => $value) {
            if ($value['attributes']['name'] == 'admin') {
                $list_role['mod'] = 'mod';
                $list_role['admin'] = 'admin';
                break;
            }
        }
        return view('admin')->with('list_user', $list_user)->with('list_role', $list_role);
    }

    public function postIndex(Request $request)
    {
        $userAction = Sentry::getUser();

        //  check permission create of user action
        if (!$userAction->hasAccess('create.admin')) {
            echo 'Không có quyền tao';
            exit();
        }

        // Validate password
        if ($request->password == '' || $request->password != $request->repassword) {
            $request->flash();
            return redirect('admin')->with('messenger_error', 'Password Error!')->withInput();
        }

        $params = $request->except(['repassword', 'admin', 'mod', 'user']);

        // Get permission
        if ($request->per != null) {
            $request->permissions = array();
            foreach ($request->per as $key => $value) {
                if ($value == 'create') $request->permissions['create.admin'] = 1;
                if ($value == 'view') $request->permissions['view.admin'] = 1;
                if ($value == 'update') $request->permissions['update.admin'] = 1;
                if ($value == 'delete') $request->permissions['delete.admin'] = 1;
            }
        }

        if ($request->user == null && $request->mod == null && $request->admin == null) {
            // Neu khong check role se mac dinh la role user
            $groups['user'] = 'user';
        } else {
            // Get role
            if ($request->user) $groups['user'] = 'user';
            if ($request->mod) $groups['mod'] = 'mod';
            if ($request->admin) $groups['admin'] = 'admin';
        }
        //  Get crete_by and active
        $params['create_by'] = $userAction->_id;
        if ($request->activated == 'on') $params['activated'] = true; else $params['activated'] = false;

        if ($this->_user_model->create($params, $groups)) {
            return redirect('admin')->with('messenger_success', 'Success!');
        } else {
            //$request->flash();
            return redirect('admin')->with('messenger_error', 'Save User Error!')->withInput();
        }

    }

    public function getUpdate($id, Request $request)
    {
        $user_action = Sentry::getUser();

        //  Check permission update of user action
        if (!$user_action->hasAccess('update.admin')) {
            echo 'Không có quyền update!';
            exit();
        }

        $user = $this->_user_model->getById($id);
        //  Get list role
        $rolesUser = array();
        foreach ($user->getGroups() as $key => $value) {
            $rolesUser[] = $value->name;
        };

        $permissions_user = $user->getMergedPermissions();

        // Get permission
        $persUser = array();
        if (isset($permissions_user['view.admin']) && $permissions_user['view.admin'] == 1) $persUser['view'] = true; else $persUser['view'] = false;
        if (isset($permissions_user['create.admin']) && $permissions_user['create.admin'] == 1) $persUser['create'] = true; else $persUser['create'] = false;
        if (isset($permissions_user['update.admin']) && $permissions_user['update.admin'] == 1) $persUser['update'] = true; else $persUser['update'] = false;
        if (isset($permissions_user['delete.admin']) && $permissions_user['delete.admin'] == 1) $persUser['delete'] = true; else $persUser['delete'] = false;

        // Get role can be create
        $groups = $user_action->getGroups();
        $list_role = array(
            'user' => 'user'
        );
        foreach ($groups as $key => $value) {
            if ($value['attributes']['name'] == 'admin') {
                $list_role['mod'] = 'mod';
                $list_role['admin'] = 'admin';
                break;
            }
        }

        return view('update')->with('user', $user)->with('listRole', $list_role)->with('rolesUser', $rolesUser)
            ->with('persUser', $persUser);
    }

    public function postUpdate(Request $request)
    {

        $params = $request->except(['_token', 'rol', 'view', 'create', 'update', 'delete']);

        // Get permission
        $params['per'] = array();
        if ($request->view) $params['per'][] = 'view.admin';
        if ($request->create) $params['per'][] = 'create.admin';
        if ($request->update) $params['per'][] = 'update.admin';
        if ($request->delete) $params['per'][] = 'delete.admin';

        if (!isset($request->rol)) {
            // Neu khong chon Role nao se mac dinh la role user
            $groups['user'] = 'user';
        } else {
            // Get role
            foreach ($request->rol as $key => $value) {
                if ($value == 'user') $groups['user'] = 'user';
                if ($value == 'mod') $groups['mod'] = 'mod';
                if ($value == 'admin') $groups['admin'] = 'admin';
            }
        }

        if ($request->activated == 'on') $params['activated'] = true; else $params['activated'] = false;

        if ($this->_user_model->update($params, $groups)) {
            return redirect('admin')->with('messenger_success', 'Success!');
        } else {
            //$request->flash();
            return redirect('admin')->with('messenger_error', 'Save User Error!')->withInput();
        }
    }

    public function getDelete($id)
    {
        $user_action = Sentry::getUser();
        if (!$user_action->hasAccess('delete.admin')) {  // If user can delete
            $data = [
                'status' => false,
                'messege' => 'Khong co quyen xoa!'
            ];
        } else {                                        // If user can't delte
            if ($this->_user_model->deleteById($id)) {
                $data = [
                    'status' => true,
                    'message' => 'Deleted!'
                ];
            } else {
                $data = [
                    'status' => false,
                    'messege' => 'Error!'
                ];
            };
        }

        return response()->json($data);
    }

    /*public function getSession() {
        $group_1_name = 'no.view';
        $group_1_permission = array(
            'view.admin' => -1,
        );
        $this->_group_model->create($group_1_name, $group_1_permission);
        $user = Sentry::findUserByID('5688ee483d11387f223486ac');
//        Session::put('phuc', 'fuk');
        echo Session::get('phuc');
    }*/
}